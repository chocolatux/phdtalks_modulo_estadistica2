<div id="menuVideos" class="ui borderless menu">
    <div class="item">
        <p class="txtAzul">Filtrar videos por:</p>
    </div>

    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento' : '')?>" class="item">
        <div class="ui mini image">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/clock.png">
        </div>
        <div class="middle aligned content txtAzul bordeAzul">
            Videos más recientes
        </div>
    </a>

    <div id="menuAreas" class="ui menu transparente">
        <div class="ui dropdown item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/areas.png">
            </div>
            <div class="middle aligned content txtAzul bordeAzul">
                Áreas del Conocimiento
            </div>
            <i class="dropdown icon"></i>
            <div id="dropdownAreas" class="menu">
                <?foreach($areasConocimiento as $area):?>
                    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento/videos/area?id=' . $area['id'] : $config->get('baseUrl') . 'videos/area?id=' . $area['id'])?>" class="item"><? echo $area['nombre'];?></a>
                <?endforeach;?>
            </div>
        </div>
    </div>

    <div id="menuEmprendimiento" class="ui right menu transparente">
        <a href="<?echo($config->get('baseUrl'))?>" id="itemEmprendimiento" class="item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/videoResumenes.png">
            </div>
            <p class="txtAzul">Video-resúmenes de papers</p>
        </a>
    </div>
</div>

<div id="buscarVideos" class="section">
    <h2 class="ui header aligned centered tituloGeneral desktop-only">
        Buscar Videos
    </h2>
</div>
<div class="ui basic segment">
    <div id="mensajeBusqueda" class="ui message large">
        <p>Se encontraron <strong><?echo($busqueda['totalVideo'])?></strong> videos que coinciden con la búsqueda: <strong><?echo($busqueda['palabra'])?></strong></p>
    </div>

    <div id="listaVideosBuscar" class="ui stackable three column grid container left aligned">
        <? foreach($busqueda['videos'] as $aVideosEncontrados){?>
            <?$aIdVideo = explode("=", $aVideosEncontrados['liga_youtube'])?>
            <div class="column">
                <div id="cardImg" class="ui card aligned center">
                    <a class="image" href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento/video/visualizar?id=' . $aVideosEncontrados['id'] : $config->get('baseUrl') . 'video/visualizar?id=' . $aVideosEncontrados['id'])?>">
                        <img class="imagPhd" src="https://img.youtube.com/vi/<?echo($aIdVideo[1])?>/mqdefault.jpg">
                    </a>
                    <div id="datosVideos" class="content left ">
                        <a class="header" href="<?echo($config->get('baseUrl'))?>video/visualizar?id=<?echo($aVideosEncontrados['id'])?>">“<?echo($aVideosEncontrados['titulo'])?>”</a>
                        <div class="description">
                            <p>Investigador: <span class="txtSubcribe"><?echo($aVideosEncontrados['nombre']) . ' ' . $aVideosEncontrados['apellido_p'] . ' ' . $aVideosEncontrados['apellido_m']?></span><br>
                                Área del conocimiento: <span class="txtSubcribe"</span><?echo ($aVideosEncontrados['areaConocimiento'])?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
