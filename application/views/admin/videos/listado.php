<div>
    <h1 class="center aligned azulMarino">Bienvenid@ <?echo(Session::get('nombre'))?></h1>

    <div class="ui container">

        <?if((!Session::get('idPerfil')) == '1'):?>
        <div id="btnAgregarVideo">
            <a class="ui basic button btnAdmin" href="<?echo($config->get('baseUrl'))?>admin/videos/agregar">
                <p>Agregar un nuevo Video</p>
            </a>
        </div>
        <?endif;?>

        <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
            <div id="mensajeExito" class="ui success message" style="display:  <?php echo($oNotices ? 'block' : '')?>">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oNotices as $sNotice):?>
                        <li><? echo($sNotice)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>

        <table id="tablaBienvenida" class="ui table striped selectable">
            <thead>
            <tr>
                <th width="300">Título</th>
                <th>Fecha de publicación</th>
                <th><i class="phdFont icon phd-visitas big"></i>Visitas</th>
                <th><i class="phdFont icon phd-play big"></i>Reproducciones</th>
                <th><i class="phdFont icon phd-clic big"></i>Clics al Artículo</th>
                <th width="80">Acciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!--mensaje emergente-->
<div id="modalEliminar" class="ui small modal">
    <i class="close icon"></i><!--cierra el icono-->
    <div class="ui header azul barra">
        Eliminar Video
    </div>
    <div class="contenido">
        <p>
            Confirme que desea eliminar el video <strong class="tituloVideo"></strong> . <br/>
            Esta operación no se puede deshacer, asegúrese de que realmente desea eliminar el video.
        </p>
    </div>
    <div class="actions">
        <div class="ui button deny btnPhd amarillo">
            <i class="remove icon"></i>
            Cancelar
        </div>
        <button id="btnEliminarVideo" class="ui button btnPhd azul" data-video="">
            <i class="trash icon"></i>
            Eliminar video
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        var dt = $('#tablaBienvenida').DataTable({
            /*checar datos*/
            "ajax": "<?echo($config->get('baseUrl'))?>jsonVideos",
            bAutoWidth: false,
            "language": {
                url: '<?echo($config->get('baseUrl'))?>assets/json/datatables.es.json'
            },
            "columns": [
                {"data": "titulo"},
                {"data": "fecha_captura"},
                {"data": "visitas"},
                {"data": "reproducciones"},
                {"data": "clics_articulo"},
                {
                    render: function (data, type, rowData, meta){
                        return '<div class="ui small basic icon buttons">' +
                            '<a class="ui icon basic button" href="<?echo($config->get('baseUrl') . 'videos/editar?video=')?>' + rowData.id + '" title="Editar"><i class="icon edit black" aria-hidden="true"></i></a>' +
                            '<button class="ui icon basic button btn-eliminar" type="button" title="Eliminar"><i class="icon remove black"></i></button>' +
                            '</div>'
                    }
                }
            ]
        });

        $('#tablaBienvenida tbody').on('click' , 'tr td .btn-eliminar' , function() {
            var tr = $(this).closest('tr');
            var row = dt.row(tr);

            /*mensaje emergente*/
            $('#modalEliminar').modal({
                onShow: function (callback) {
                    var $tituloVideo = $(this).find('.contenido').find('.tituloVideo');
                    /*busca en la clase contenido y busca titulovideo*/
                    $tituloVideo.html(row.data().titulo);
                    var $btnEliminar = $(this).find('.actions').find('#btnEliminarVideo');
                    /*busca la clase acción y busca el id del boton eliminar video */
                    $btnEliminar.attr('data-video', row.data().id);
                    /*recuperar el valor del atributo*/
                }
            }).modal('show');
            /*muestra el mensaje*/
        });

        /*acción de eliminar*/
        $("#btnEliminarVideo").click(function(){
            var video = $(this).data('video');

            if(video == undefined) /*si eo video no es definido*/
            {
                alert("No se ha especificado el video a eliminar");
                return false
            }
            $.post('<?echo ($config->get("baseUrl"))?>videos/ajaxEliminarVideo',{ video : video },function (data){

                if(data.success !== true) /*verificar si tuvo exito*/
                {
                    return false;
                }
                window.location.reload();/*actualiza datos en momento real*/

                $('#modalEliminar').modal("hide");

            }, 'json');
        });

    });
</script>