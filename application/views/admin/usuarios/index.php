<div id="adminPhd">
    <h1 class="center aligned azulMarino">Bienvenid@ <?echo(Session::get('nombre'))?></h1>

        <div id="menuGeneralAdmin" class="ui container">
            <div  class="ui grid stackable centered">
                <div  class="five wide column contenedorBtn bordeAzul">
                    <div class=" blueBackground">
                        <div class="header txtHeader">Investigadores</div>
                    </div>
                    <div class="ui items">
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/investigadores/registrar"><i class="icon add"></i>Registrar nuevo investigador</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/investigadores/listado"><i class="icon list"></i>Administrar Investigadores</a>
                                    <p class="txtBordeInferior">Videos</p>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/video/agregar"><i class="icon add"></i>Registrar nuevo video de Investigador</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/videos/listado"><i class="icon list"></i>Administrar videos de Investigadores</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="five wide column contenedorBtn bordeAzul">
                    <div class="ui items">
                        <div class="content blueBackground">
                            <div class="header txtHeader">Emprendedores Científicos</div>
                        </div>
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content ">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/emprendedor/registrar"><i class="icon add"></i>Registrar nuevo Emprendedor</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/emprendedores/listado"><i class="icon list"></i>Administrar emprendedores</a>
                                    <p class="txtBordeInferior">Videos</p>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/emprendedores/video/agregar"><i class="icon add"></i>Registrar nuevo video de Emprendedor</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/emprendedores/videos/listado"><i class="icon list"></i>Administrar videos de Emprendedores</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="five wide column contenedorBtn">
                    <div class="ui items bordeAmarillo admin">
                        <div class="content yellowBackground">
                            <div class="header txtHeader txtNegro">Administradores</div>
                        </div>
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content">
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/usuarios/registrar"><i class="icon add"></i>Registrar nuevo Administrador</a>
                                    <a class="ui basic black fluid large button" href="<?echo($config->get('baseUrl'))?>admin/usuarios/listado"><i class="icon list"></i>Administrar usuarios Administradores</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ui items bordeAmarillo">
                        <div class="content yellowBackground">
                            <div class="header txtHeader">Estadística</div>
                        </div>
                        <div class="item">
                            <div class="middle aligned content">
                                <div class="content">
                                    <a class="ui basic black fluid large button btnReporte" href="<?echo($config->get('baseUrl'))?>admin/estadistica/resumen"><img class="barra" src="<?echo($config->get('baseUrl'))?>assets/img/bar.png"><p>Reportes</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

</div>