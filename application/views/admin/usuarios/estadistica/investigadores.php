<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../psd3.css" />
</head>

<body style="margin: 15px;">

    <div class="container">
        <div class="well">
        </div>
        <div style="text-align:center;">
        <div id="chartContainer" class="well"></div>
    </div>

    <script type="text/javascript">
        var sampleData = [{
            noOfColleges: 100,
            place: "Total",
            drilldown: [{
                noOfColleges: 30,
                place: "Investigadores",
                drilldown: []
            },{
                    noOfColleges: 15,
                    place: "Usuarios",
                    drilldown: []
                } ]
        },
         ];
        var config = {
            containerId: "chartContainer",
            width: 200,
            height: 200,
            data: sampleData,
            heading: {
                text: "Usuarios activos",
                pos: "top"
            },
            label: function(d) {
                return d.data.place + ":" + d.data.noOfColleges;
            },
            value: "noOfColleges",
            inner: "drilldown",
            tooltip: function(d) {
                return "<div style='background-color: #4a4; color: white; padding: 15px; text-align: middle; border: dotted 1px black;'><strong>" + d.place + "</strong> has " + d.noOfColleges + " Usuarios .</div>";
            },
            transition: "linear",
            transitionDuration: 300,
            donutRadius: 50,
            gradient: true,
            colors: d3.scale.category20(),
            labelColor: "white",
            stroke: "#eee",
            strokeWidth: 3,
            drilldownTransition: "linear",
            drilldownTransitionDuration: 0,
            highlightColor: "#c00"
        };
        var samplePie = new psd3.Pie(config);
    </script>
