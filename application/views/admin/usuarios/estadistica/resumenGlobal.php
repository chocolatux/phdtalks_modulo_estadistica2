<div id="resumenGlobal">
  <canvas id="VideoPublicados" width="300" height="100"></canvas>
  <canvas id="Visitantes" width="800" height="450"></canvas>
  <canvas id="AdquisicionUsuarios" width="300" height="100"></canvas>
</div>

<div class="container">
    <div class="well">
    </div>
      <div style="text-align:center;">
    <div id="chartContainer" class="well"></div>
</div>

<script>
  $(document).ready(function()
  {
    new Chart(document.getElementById("VideoPublicados"),
    {
      type: 'line',
      data: {
        labels: ["Ene","Feb","Marzo","Abril","Mayo","Junio","Julio","Agosto","Sept","Oct","Nov","Dic"],
        datasets: [{
            data: [1,3,2,8,5,7,9,11,10,15,14,15],
            label: "Video Publicados",
            borderColor: "#2a447d",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Histórico de Videos Publicados'
        }
      }
    });


    new Chart(document.getElementById("Visitantes"),
    {
        type: 'bar',
        data: {
          labels: ["Ene","Feb","Marzo","Abril","Mayo","Junio","Julio","Agosto","Sept","Oct","Nov","Dic"],
          datasets: [
            {
              label: "Visitantes",
              backgroundColor: ["#ecab15","#ecab15","#ecab15","#ecab15","#ecab15","#ecab15","#ecab15","#ecab15","#ecab15",],
              data: [1,2,3,4,5,6,7,8,9,10,11,12]
            }
          ]
        },
        options: {
          legend: { display: false },
          title: {
            display: true,
            text: 'Histórico de Visitantes'
          }
        }
    })


    new Chart(document.getElementById("AdquisicionUsuarios"),
    {
    type: 'line',
    data: {
      labels: ["Ene","Feb","Marzo","Abril","Mayo","Junio","Julio","Agosto","Sept","Oct","Nov","Dic"],
      datasets: [{
          data: [1,3,2,8,5,7,9,11,10,15,14,15],
          label: "Usuarios Obtenidos",
          borderColor: "#2a447d",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Adquisición de Usuarios'
      }
    }
  });

  });


  </script>

  <script type="text/javascript">
      var sampleData = [{
          noOfColleges: 100,
          place: "Total",
          drilldown: [{
              noOfColleges: 30,
              place: "Investigadores",
              drilldown: []
          },{
                  noOfColleges: 15,
                  place: "Usuarios",
                  drilldown: []
              } ]
      },
       ];
      var config = {
          containerId: "chartContainer",
          width: 200,
          height: 200,
          data: sampleData,
          heading: {
              text: "Usuarios activos",
              pos: "top"
          },
          label: function(d) {
              return d.data.place + ":" + d.data.noOfColleges;
          },
          value: "noOfColleges",
          inner: "drilldown",
          tooltip: function(d) {
              return "<div style='background-color: #4a4; color: white; padding: 15px; text-align: middle; border: dotted 1px black;'><strong>" + d.place + "</strong> has " + d.noOfColleges + " Usuarios .</div>";
          },
          transition: "linear",
          transitionDuration: 300,
          donutRadius: 50,
          gradient: true,
          colors: d3.scale.category20(),
          labelColor: "white",
          stroke: "#eee",
          strokeWidth: 3,
          drilldownTransition: "linear",
          drilldownTransitionDuration: 0,
          highlightColor: "#c00"
      };
      var samplePie = new psd3.Pie(config);
  </script>
