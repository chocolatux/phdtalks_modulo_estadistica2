<p class="ui tituloBarra azulMarino"><?echo($bActualizar == '1' ? 'Actualizar' : 'Registrar nuevo')?> Administrador</p>
<div class="fondo gris">

    <?if($oErrors):?>
        <div class="ui error message">
            <i class="close icon"></i>
            <ul class="list">
                <?foreach($oErrors->mensajes as $sError):?>
                    <li><? echo($sError)?></li>
                <? endforeach;?>
            </ul>
        </div>
    <? endif;?>

    <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
        <div class="ui success message messagePerfil">
            <i class="close icon"></i>
            <ul class="list">
                <?foreach($oNotices as $sNotice):?>
                    <li><? echo($sNotice)?></li>
                <? endforeach;?>
            </ul>
        </div>
    <? endif;?>

    <form id="formularioAdmin" method="POST" class="ui container form" action="">
        <div class="fields">
            <div class="six wide field">
                <label>Nombre*</label>
                <input name="form[nombre]" value="<?echo($aUsuario['nombre'])?>">
            </div>
            <div class="six wide field">
                <label>Apellido Paterno*</label>
                <input name="form[apellido_p]" value="<?echo($aUsuario['apellido_p'])?>">
            </div>
            <div class="six wide field">
                <label>Apellido Materno*</label>
                <input name="form[apellido_m]" value="<?echo($aUsuario['apellido_m'])?>">
            </div>
        </div>
        <div class="field">
            <label>Correo*</label>
            <input name="form[email]" value="<?echo($aUsuario['email'])?>">
        </div>
        <div class="fields">
            <div class="eight wide field">
                <label>Contraseña</label>
                <input name="form[password]" type="password">
            </div>
            <div class="eight wide field">
                <label>Repite la contraseña</label>
                <input name="form[rtpassword]" type="password">
            </div>
        </div>

        <div class="ui error message"></div>

        <div class="center btnSepara">
            <p>Los campos marcados con * son obligatorios.</p>
            <button class="ui button registroUsuario btnAzul" type="submit">Guardar mi perfil</button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){

        $('#formularioAdmin').form({
            on: 'blur',
            fields: {
                nombre: {
                    identifier: 'form[nombre]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'El nombre no es válido'
                        }
                    ]
                },
                apellidoPaterno: {
                    identifier: 'form[apellido_p]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'El apellido paterno no es válido'
                        }
                    ]
                },
                apellidoMaterno: {
                    identifier: 'form[apellido_m]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'El apellido materno no es válido'
                        }
                    ]
                },
                correoPersonal: {
                    identifier: 'form[email]',
                    rules: [
                        {
                            type: 'email',
                            prompt: 'El correo personal ingresado no es válido'
                        }
                    ]
                }
                <?if(!$bActualizar){?>,
                password: {
                    identifier: 'form[password]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Debe especificar la contraseña'
                        }
                    ]
                },
                rtpassword: {
                    identifier  : 'form[rtpassword]',
                    rules: [
                        {
                            type   : 'match[form[password]]',
                            prompt : 'Las contraseñas no coinciden'
                        },
                        {
                            type: 'empty',
                            prompt: 'Vuelva a escribir la constraseña'
                        }
                    ]
                }

                <?}?>

            }
        });

    });
</script>