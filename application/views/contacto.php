<p class="ui tituloBarra azulMarino">Contacto</p>

<div class="fondo gris">

<form id="contactoForm" class="ui container form" action="" method="POST">

    <div id="mensajeExito" class="center aligned">
        <p>
            Muchas gracias por "tus comentarios. <br />
            Muy pronto nos pondremos en contacto contigo.
        </p>
        <div class="center aligned">
            <a href="<?echo($config->get('baseUrl'))?>" class="ui button btnYellow" type="button">
                Volver a los videos
            </a>
        </div>
    </div>

<div class="ui field">
    <div class="field">
        <label>Nombre*</label>
        <input name="form[nombre]">
    </div>
    <div class="field">
        <label>Correo Electrónico*</label>
        <input name="form[email]">
    </div>
    <div class="field">
        <label>Comentarios*</label>
        <textarea id="comentario" name="form[comentarios]"></textarea>
    </div>
</div>
    <div class="center">
        <p>Los campos marcados con * son obligatorios.</p>
        <button class="large ui button registroUsuario btnYellow" type="submit">Enviar</button>
    </div>
    <div class="ui error message"></div>

</form>

</div>

<script>
    $(document).ready(function(){
        $('#contactoForm').form({
            fields: {
                nombre: {
                    identifier: 'form[nombre]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Es necesario capturar su nombre'
                        }
                    ]
                },
                email: {
                    identifier: 'form[email]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Es necesario capturar su correo electrónico'
                        },
                        {
                            type: 'email',
                            prompt: 'El correo electrónico no es válido'
                        }
                    ]
                },
                comentarios: {
                    identifier: 'form[comentarios]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Es necesario capturar el mensaje'
                        }
                    ]
                }
            },
            on: 'blur',
            transition: 'fade down',
            onSuccess: function(e){
                e.preventDefault();
                var $formContacto = $(this);
                $formContacto.addClass('loading');
                $.post("/contacto", $formContacto.serialize(), function(data){
                    setTimeout(function() {
                        $formContacto.removeClass('loading');
                        $formContacto.addClass('enviado');
                        $formContacto[0].reset();
                        $formContacto.find('#mensajeExito').show();

                    }, 1000);
                }, "json");
            }
        })
    });
</script>