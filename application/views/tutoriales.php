<p class="ui tituloBarra azulMarino">Guía y Tutoriales para grabar tus video-resúmenes</p>

<div id="contenedor" class="ui container">
    <p>Para que tu video-resumen tenga una estructura clara y entendible, te recomendamos que contenga la siguiente información en los siguientes tiempos recomendados:</p>

    <div class="center aligned">
        <table id="tablaTutorial" class="ui very basic table">
            <thead>
            <tr>
                <th id="columnaTiempo">Tiempo</th>
                <th id="columnaElementos">Elementos</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="txtAmarillo">10 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>1.   Título del artículo e Introducción</td>
            </tr>
            <tr>
                <td class="txtAmarillo">20 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>2.   ¿Cuál es el objetivo del artículo / la pregunta de investigación que plantea?</td>
            </tr>
            <tr>
                <td class="txtAmarillo">20 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>3.   ¿Cuál es la justificación de la investigación? ¿Por qué es importante?</td>
            </tr>
            <tr>
                <td class="txtAmarillo">20 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>4.   ¿Qué metodología utilizó en caso de ser un artículo empírico?</td>
            </tr>
            <tr>
                <td class="txtAmarillo">20 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>5.   ¿Cuáles son los principales hallazgos / contribuciones descritos en el artículo?</td>
            </tr>
            <tr>
                <td class="txtAmarillo">20 s  &nbsp; &nbsp; &nbsp; - </td>
                <td>6.   ¿Cuáles son algunas implicaciones prácticas? (en caso de haber)</td>
            </tr>
            </tbody>
        </table>

    </div>

    <p>Tutorial para Windows:</p>

    <div class="row">
        <div id="videoTutorial" class="center aligned">
            <div class="video-responsive">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/9ZlAhPpN5GU" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>


</div>