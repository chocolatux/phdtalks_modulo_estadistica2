<div id="footerColor">
    <div class="ui stackable grid" id="gridFooter">
        <div id="contenidoFooter" class="row middle aligned">
            <div class="left aligned eight wide column">
                <div class="ui items">
                    <div class="item" id="itemFooter">
                        <div class="ui image">
                            <a href="http://www.chocolatux.com">
                                <img src="<?echo($config->get('baseUrl'))?>assets/img/logoChoco.png">
                            </a>
                        </div>
                        <div class="middle aligned content">
                            <p class="txtFooter">Derechos Reservados PHD Talks 2017. Guadalajara, Jalisco, México.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right aligned eight wide column">
                <a class="item txtFooter" href="<?echo($config->get('baseUrl'))?>servicios">Servicios</a>
                <span class="item txtFooter">|</span>
                <a class="item txtFooter" href="<?echo($config->get('baseUrl'))?>acercade">Acerca de PHD Talks</a>
                <span class="item txtFooter">|</span>
                <a class="item txtFooter" href="<?echo($config->get('baseUrl'))?>contacto">Contacto</a>
                <span class="item txtFooter">|</span>
                <a class="item txtFooter" href="<?echo($config->get('baseUrl'))?>avisoPrivacidad">Aviso de privacidad</a>
                <span class="item txtFooter">|</span>
                <a class="item txtFooter" href="<?echo($config->get('baseUrl'))?>avisoLegal">Aviso legal</a>
            </div>
        </div>
    </div>
</div>
