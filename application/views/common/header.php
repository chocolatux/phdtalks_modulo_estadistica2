<div id="fixedmenu" class="ui top fixed">
    <div id="barraMenu">
        <a href="<?echo($config->get('baseUrl'))?>">
            <img class="ui fluid small left floated image" id="logoPhd" src="<?echo($config->get('baseUrl'))?>assets/img/logo.png">
        </a>

            <div id="menuAcercaDe" class="ui menu">
                <div class="right menu">
                    <a href="<?echo($config->get('baseUrl'))?>servicios" class="item ocultable">Servicios</a>
                    <span class="item ocultable">|</span>
                    <a href="<?echo($config->get('baseUrl'))?>acercade" class="item ocultable">Acerca de PhdTalks</a>
                    <?if(!Session::get('idUsuario')):?>
                        <a id="txtRegistrate" class="item" href="<?echo $bEmprendimiento == 1 ? ($config->get('baseUrl')) . 'emprendedor/registrar' : 'investigador/registrar'?>">Regístrate aquí</a>
                    <?endif;?>
                </div>
            </div>


        <div id="gridHeaderResponsivo" class="ui grid center aligned">
            <div class="row">
                <div id="textoBuscador" class="txtBlanco"><?echo $bEmprendimiento == 1 ? '¡Impulsemos el emprendimiento basado en la ciencia!' : 'En PHD Talks puedes encontrar artículos científicos y escuchar a sus autores explicarlos en tan solo 3 minutos'?></div>
            </div>
            <div class="row">

                <div id="buscadorHeader" class="ui fluid icon input">
                    <input class="inputBuscador" placeholder="Busca por tema, investigador o disciplina" type="text">
                    <i class="btnBuscador search link icon"></i>
                </div>
            </div>
        </div>

        <div id="menuInvestigador" class="ui secondary menu">
            <div id="click" class="ui floating cel">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/Cel.png">
            </div>
            <a id="txtAreasConocimiento" class="item">Àreas del conocimiento</a>
            <div class="ui left demo sidebar vertical inverted menu" id="sidebarMenu2">
                <a class="item" href="">Videos más recientes</a>
                <div class="item">
                    <b>Áreas del Conocimiento</b>
                    <div class="menu">
                        <? foreach($areasConocimiento as $area) {?>
                            <a href="<?echo($config->get('baseUrl'))?>videos/area?id=<?echo($area['id'])?>" class="item optMenu2"><? echo $area['nombre'];?></a>
                        <?}?>
                    </div>
                </div>
                <a class="item" href="<?echo($config->get('baseUrl'))?>servicios">Servicios</a>
                <a class="item" href="<?echo($config->get('baseUrl'))?>tutoriales">Tutoriales</a>
                <a class="item" href="<?echo($config->get('baseUrl'))?>acercade">Acerca de PhdTalks</a>
            </div>

            <div class="menu">
                <h4 class="ocultable item"><?echo $bEmprendimiento == 1 ? 'Emprendedores Científicos:' : 'Investigadores:'?></h4>
            </div>
            <div class="right menu">
                <a href="<?echo($config->get('baseUrl'))?>tutoriales" class="ocultable item">Tutoriales</a>
                <?if(!Session::get('idUsuario')):?>
                    <span class="ocultable item">|</span>
                    <div class="ocultable item"><p><a class="txtUnderline" href="<?echo $bEmprendimiento == 1 ? $config->get('baseUrl') . 'emprendedor/registrar' : $config->get('baseUrl') . 'investigador/registrar'?>">Regístrate aquí</a><?echo $bEmprendimiento == 1 ? ' y difunde tu proyecto en PHDTalks' : ' y difunde tus papers en PHDTalks'?></p></div>
                <?endif;?>
                <?if(Session::get('idUsuario')):?>
                    <div class="ui dropdown item bordeAzulIzquierdo">
                        <i class="ui icon large user"></i>
                        <?echo(Session::get('nombre')); if(Session::get('idPerfil') == '1'): echo(' (Administrador)'); elseif(Session::get('idPerfil') == '2'): echo(' (Investigador)'); elseif(Session::get('idPerfil') == '3'): echo(' (Emprendedor)'); endif;?>
                        <i class="dropdown white icon"></i>

                        <div class="ui menu">
                            <?if(Session::get('idPerfil') == '1'): ?>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>admin/misdatos">Actualizar mi Perfil</a>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>admin/index">Panel Administrativo</a>
                            <?elseif(Session::get('idPerfil') == '2'):?>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/misdatos">Actualizar mi Perfil</a>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/index">Administrar mis videos</a>
                            <?elseif(Session::get('idPerfil') == '3'):?>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>emprendedor/misdatos">Actualizar mi Perfil</a>
                                <a class="item" href="<?echo($config->get('baseUrl'))?>emprendedor/index">Administrar mis videos</a>
                            <?endif;?>
                            <a class="item" href="<?echo($config->get('baseUrl'))?>logout">Salir</a>
                        </div>
                    </div>

                <?else:?>
                    <a href="<?echo($config->get('baseUrl'))?>login" class="item bordeAzulIzquierdo">Iniciar Sesión</a>
                <?endif;?>

            </div>
        </div>

        <div id="gridHeader" class="ui grid center aligned">

            <div class="row">
                <div id="textoBuscador" class="txtBlanco"><?echo $bEmprendimiento == 1 ? '¡Impulsemos el emprendimiento basado en la ciencia!' : 'En PHD Talks puedes encontrar artículos científicos y escuchar a sus autores explicarlos en tan solo 3 minutos' ?></div>
            </div>
            <div class="row center aligned">

                <div id="buscadorHeader" class="ui fluid icon input">
                    <input class="inputBuscador" placeholder="Busca por tema, investigador o disciplina" type="text">
                    <i class="btnBuscador search link icon"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function() {

        $(".inputBuscador").keyup(function(evento){
            if(evento.keyCode == 13) //ASCII de enter
            {
                $(this).next().click();
            }
        });

        $(".btnBuscador").click(function(e){
            var palabra = $(this).prev().val();

            if(palabra != '')
            {
                window.location = '<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento/videos/buscar?r=' : $config->get('baseUrl') . 'videos/buscar?r=')?>' + encodeURIComponent(palabra);
            }
        });

        semantic.menu.ready();

        $('#click').click(function()
            {
                $('.left.demo.sidebar')
                    .sidebar('toggle')
            }
        );

        $('#txtAreasConocimiento').click(function()
            {
                $('.left.demo.sidebar')
                    .sidebar('toggle')
            }
        );
    });
</script>