<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>PHD Talks - Portal del artículos científicos</title>

    <link rel="icon" href="<?echo($config->get('baseUrl'))?>assets/img/favicon.ico" type="image/x-icon" />

    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/semantic.min.css"> <!--calendario-->
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/dataTables.semanticui.min.css">


    <script src="<?echo($config->get('baseUrl'))?>assets/js/jquery.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/semantic.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/jquery.dataTablesn.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/dataTables.semanticui.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/chart.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/psd3.min.js"></script>
    <script src="<?echo($config->get('baseUrl'))?>assets/js/d3.min.js"></script>


</head>
<body>
<div id="header">
    <?require_once ('headerSistema.php')?>
</div>

<?php $this->show($sSectionFile, get_defined_vars()['vars'])?>

</body>
</html>
