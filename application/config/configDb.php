<?php
$config = Config::getInstance();

$config->set('dbhost', 'localhost');
$config->set('dbname', 'phdtalks');
$config->set('dbuser', 'root');
$config->set('dbpass', '');
$config->set('baseDomain', 'http://phdtalks-bare.local/');
