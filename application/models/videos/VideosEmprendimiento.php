<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 11/11/17
 * Time: 12:16
 */
class VideosEmprendimiento extends ModelBase
{
    protected $sTable = "videos_emprendimiento";
    protected $sPrimaryKey = 'id';

    private static function obtenerReproduccionesVideo($sIdVideoYoutube, $mVideos)
    {
        $oModelo = new static();

        $JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id={$sIdVideoYoutube}&key=AIzaSyAQ_qI3Jio--0DUMoQxO_YYtVuQkrZ4ngw");

        $JSON_data = json_decode($JSON);

        foreach($mVideos as $key => $aVideo)
        {
            $mVideos[$key]['reproducciones'] = $JSON_data->items[$key]->statistics->viewCount;
        }

        return $mVideos;
    }

    public static function obtenerVideosRecientesEmprendimiento($nVideoInicial = 0)
    {
        $oModelo = new static();

        $sQuery = "SELECT videos_emprendimiento.id, videos_emprendimiento.titulo, videos_emprendimiento.id_emprendedor, emprendedores.nombre, emprendedores.apellido_p, emprendedores.apellido_m,
        videos_emprendimiento.liga_youtube, subareas_conocimiento.nombre as Subarea, areas_conocimiento.nombre as areaConocimiento
        FROM videos_emprendimiento
        INNER JOIN emprendedores ON videos_emprendimiento.id_emprendedor = emprendedores.id
        INNER JOIN areas_conocimiento ON videos_emprendimiento.id_area_conocimiento = areas_conocimiento.id 
        INNER JOIN subareas_conocimiento ON videos_emprendimiento.id_subarea_conocimiento = subareas_conocimiento.id 
        WHERE videos_emprendimiento.`ON` = 1 AND videos_emprendimiento.bVisible = 1
        ORDER BY videos_emprendimiento.fecha_captura DESC 
        LIMIT {$nVideoInicial},9";

        if(!$mVideosRecientes = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        foreach ($mVideosRecientes as &$aVideoReciente)
        {
            parse_str(parse_url($aVideoReciente['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);
            $aVideoReciente['imagen'] = $aYoutubeParams['v'];
        }

        return $mVideosRecientes;
    }

    public static function agregarVideo($aDatosVideo)
    {
        //Si existe el video se edita sino se crea
        if($aDatosVideo['id'])
        {

            if(!$oVideo = VideosEmprendimiento::findById($aDatosVideo['id']))
            {
                return false;
            }

            $oVideo->update($aDatosVideo);

        }else{

            $oVideo = VideosEmprendimiento::create($aDatosVideo);

        }

        return $oVideo->id;

    }

    public static function obtenerVideos($nIdEmprendedor)
    {
        $oModelo = new static();
        $sIdsVideos = '';

        $sQuery = "SELECT videos_emprendimiento.id, videos_emprendimiento.bVisible, videos_emprendimiento.titulo,
                   videos_emprendimiento.descripcion , videos_emprendimiento.liga_youtube, videos_emprendimiento.liga_resumen,
                   videos_emprendimiento.fecha_captura, videos_emprendimiento.esquema_proteccion, areas_conocimiento.nombre as AreaConocimiento,
                   subareas_conocimiento.nombre as Subarea, videos_emprendimiento.palabras_clave, videos_emprendimiento.visitas, videos_emprendimiento.clics_articulo, videos_emprendimiento.id_video_youtube
                   FROM videos_emprendimiento
                   INNER JOIN areas_conocimiento ON videos_emprendimiento.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento ON videos_emprendimiento.id_subarea_conocimiento = subareas_conocimiento.id
                   WHERE videos_emprendimiento.`ON` = 1
                    ";

        //Si no tiene los permisos de admin se obtienen sólo los videos de un investigador
        if(!((Session::get('idPerfil')) == '1'))
        {
            $sQuery .= " AND id_emprendedor = {$nIdEmprendedor}";
        }
        
        if(!$mVideos = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        //Se convierten la fecha y la hora a formato humano y se obtiene la reproducción de cada video
        foreach($mVideos as $key => $aVideo)
        {
            $mVideos[$key]['fecha_captura'] = Utils::formatDateTimeToHuman($aVideo['fecha_captura'], $oModelo->_config->get('formatoFechaNombreMes'));

            //Se concatenan los id de los videos para consultar sus estadisticas
            $sIdsVideos .= $mVideos[$key]['id_video_youtube'];

            //Si no es el último video separar los id por comas
            if($key != count($mVideos)-1)
            {
                $sIdsVideos .= ',';
            }

            $mVideos = $oModelo->obtenerReproduccionesVideo($sIdsVideos, $mVideos);
        }

        return $mVideos;
    }

    public static function obtenerInfoVideo($nIdVideo)
    {
        $oModelo = new static();


        $sQuery = "SELECT videos_emprendimiento.id, videos_emprendimiento.id_subarea_conocimiento, videos_emprendimiento.bVisible, videos_emprendimiento.titulo, videos_emprendimiento.descripcion , videos_emprendimiento.liga_youtube, videos_emprendimiento.liga_resumen, videos_emprendimiento.fecha_captura, videos_emprendimiento.esquema_proteccion, areas_conocimiento.nombre as area, subareas_conocimiento.nombre as subArea, videos_emprendimiento.palabras_clave,
                   emprendedores.nombre, emprendedores.apellido_p, emprendedores.apellido_m, videos_emprendimiento.clics_articulo, videos_emprendimiento.visitas
                   FROM videos_emprendimiento
                   INNER JOIN emprendedores ON videos_emprendimiento.id_emprendedor = emprendedores.id
                   INNER JOIN areas_conocimiento ON videos_emprendimiento.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento ON videos_emprendimiento.id_subarea_conocimiento = subareas_conocimiento.id
                   WHERE videos_emprendimiento.`ON` = 1 AND videos_emprendimiento.id = {$nIdVideo}";

        if(!$aVideo = $oModelo->_db->getOne($sQuery))
        {
            return false;
        }

        //Se obtiene el ID del video de youtube
        parse_str(parse_url($aVideo['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);

        //Se obtienen las reproducciones totales de youtube
        $aVideo['reproducciones'] = $oModelo->obtenerReproduccionesVideo($aYoutubeParams['v'], $aVideo);

        return $aVideo;
    }

    public static function eliminarVideo($nIdVideo)
    {
        if(!$nIdVideo)
        {
            return array('success' => false, 'msg' => 'No se especificó el video');
        }

        /*Se busca el video*/
        if(!$oVideo = self::findById($nIdVideo))
        {
            return array('success' => false, 'msg' => 'Video no encontrado en la base de datos');
        }

        if(!empty($oVideo))
        {
            $oVideo->ON = 0;
            $oVideo->save();
        }
        return array ('success' => true);
    }

    public static function buscarVideo($sPalabra)
    {
        $oModelo = new static();

        $sQuery = "SELECT videos_emprendimiento.id, videos_emprendimiento.titulo, videos_emprendimiento.descripcion , videos_emprendimiento.liga_youtube, videos_emprendimiento.liga_resumen, videos_emprendimiento.fecha_captura, areas_conocimiento.nombre as areaConocimiento, subareas_conocimiento.nombre as Subarea, videos_emprendimiento.palabras_clave,emprendedores.nombre AS nombre, emprendedores.apellido_p, emprendedores.apellido_m
                   FROM videos_emprendimiento
                   INNER JOIN areas_conocimiento ON videos_emprendimiento.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento ON videos_emprendimiento.id_subarea_conocimiento = subareas_conocimiento.id
                   INNER JOIN emprendedores ON videos_emprendimiento.id_emprendedor = emprendedores.id
                   WHERE videos_emprendimiento.`ON` = 1 AND videos_emprendimiento.bVisible = 1 AND (videos_emprendimiento.titulo LIKE '%$sPalabra%' OR videos_emprendimiento.palabras_clave LIKE '%$sPalabra%' OR emprendedores.nombre LIKE '%$sPalabra%' OR areas_conocimiento.nombre LIKE '%$sPalabra%' OR emprendedores.apellido_p LIKE '%$sPalabra%' OR emprendedores.apellido_m LIKE '%$sPalabra%')";

        $mVideos = $oModelo->_db->getAll($sQuery);

        return array('videos' => $mVideos, 'totalVideo' => count($mVideos));
    }

    public static function obtenerVideosPorArea($nIdArea)
    {
        $oModelo = new static();
        $sQuery ="SELECT videos_emprendimiento.id, emprendedores.nombre, emprendedores.apellido_p, emprendedores.apellido_m, videos_emprendimiento.titulo, videos_emprendimiento.descripcion, videos_emprendimiento.liga_youtube, videos_emprendimiento.liga_resumen, subareas_conocimiento.nombre as subArea, videos_emprendimiento.palabras_clave, areas_conocimiento.nombre  as areaConocimiento
                  FROM videos_emprendimiento
                  INNER JOIN emprendedores ON videos_emprendimiento.id_emprendedor = emprendedores.id
                  INNER JOIN areas_conocimiento ON videos_emprendimiento.id_area_conocimiento = areas_conocimiento.id
                  INNER JOIN subareas_conocimiento ON videos_emprendimiento.id_area_conocimiento = subareas_conocimiento.id
                  WHERE videos_emprendimiento.`ON` = 1 AND areas_conocimiento.id = {$nIdArea} AND videos_emprendimiento.bVisible = 1
                  ORDER BY videos_emprendimiento.fecha_captura DESC";

        if(!$mVideosPorArea = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        return $mVideosPorArea;
    }

}