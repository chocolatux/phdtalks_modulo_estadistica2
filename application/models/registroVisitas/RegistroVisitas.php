<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 30/11/17
 * Time: 10:22
 */
class RegistroVisitas extends ModelBase
{
    protected $sPrimaryKey = 'id';
    protected $sTable = 'registro_visitas';

    public static function agregarRegistro($aRegistro)
    {
        $oRegistro = new static();

        if($oRegistro->create($aRegistro))
        {
            return true;
        }

        return false;
    }


}