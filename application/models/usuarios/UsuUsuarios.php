<?php
/**
 * Modelo de la tabla usu_usuarios
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Models\Usuarios
 * @author David Heredia <david@chocolatux.com>
 */

class UsuUsuarios extends ModelBase {

    /**
     * Nombre de la tabla del modelo
     * @var string
     */
    protected $sTable = 'usu_usuarios';

    /**
     * Regresa un arreglo con los datos del usuario incluyendo
     * perfil y nombre completo
     * 
     * @param int $nUsuario
     * @return array
     */
    public static function obtenerUsuarioInfo($nUsuario)
    {
        $oModel = new static;


        //Query para obtener la información del usuario
        $sQueryUsuarioPerfil = "
            SELECT usu_usuarios.ID, usu_usuarios.nombre, usu_usuarios.email, usu_usuarios.apellido_p, usu_usuarios.apellido_m, usu_usuarios.idPerfil, seg_perfiles.perfil as perfil
            FROM usu_usuarios
            INNER join seg_perfiles ON usu_usuarios.idPerfil = seg_perfiles.ID
            WHERE usu_usuarios.`ON` = 1 AND usu_usuarios.ID = '{$nUsuario}'
        ";

        return $oModel->_db->getOne($sQueryUsuarioPerfil);

    }

    public static function agregarUsuario($aUsuario)
    {
        //Se encripta la contraseña en MD5
        if($aUsuario['password'])
        {
            $aUsuario['password'] = utils::encriptMD5($aUsuario['password']);
        }

        //Si existe el usuario se actualiza, si no se crea
        if($aUsuario['ID'])
        {
            if(!$oUsuario = UsuUsuarios::findById($aUsuario['ID']))
            {
                return false;
            }

            $oUsuario->update($aUsuario);

        }else{
            //Se crea el registro del usuario
            if(!$oUsuario = self::create($aUsuario))
            {
                return false;
            }

        }

        return $oUsuario;
    }

    public static function obtenerUsuarios($nIdUsuario)
    {
        $oModel = new static;

        //Query para obtener la información del usuario
        $sQueryUsuarioPerfil = "
            SELECT
            usu_usuarios.ID, nombre, email,
            usu_usuarios.idPerfil, seg_perfiles.perfil as perfil
            FROM usu_usuarios
            INNER join seg_perfiles ON usu_usuarios.idPerfil = seg_perfiles.ID
            WHERE usu_usuarios.`ON` = 1 AND usu_usuarios.idPerfil = 1 AND usu_usuarios.ID != '{$nIdUsuario}'
        ";

        if(!$mUsuarios = $oModel->_db->getAll($sQueryUsuarioPerfil))
        {
            return false;
        }

        return $mUsuarios;

    }
    public static function eliminarUsuario($nIdUsuario)
    {
        if(!$nIdUsuario)
        {
            return array('success' => false, 'msg' => 'Error, no se especificó el usuario');
        }

        if(!$oUsuario = self::findById($nIdUsuario))
        {
            return array('success' => false, 'msg' => 'Error, no se encontró el usuario en la base de datos');
        }

        if(!empty($oUsuario))
        {
            $oUsuario->ON = 0;
            $oUsuario->save();
        }

        return array('success' => true, 'idInvestigador' => $oUsuario->idInvestigador);

    }

}
