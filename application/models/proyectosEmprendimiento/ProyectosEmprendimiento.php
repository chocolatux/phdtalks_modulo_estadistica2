<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 09/11/17
 * Time: 13:55
 */


class ProyectosEmprendimiento extends ModelBase
{
    protected $sTable = "proyectos_emprendimiento";
    protected $sPrimaryKey = "id";

    public static function agregarProyectoEmprendimiento($aProyectoEmprendimiento)
    {
        if($aProyectoEmprendimiento['id'])
        {
            if(!$oProyectoEmprendimiento = ProyectosEmprendimiento::findById($aProyectoEmprendimiento['id']))
            {
                return false;
            }

            $oProyectoEmprendimiento->update($aProyectoEmprendimiento);

        }else{

            $oProyectoEmprendimiento = self::create($aProyectoEmprendimiento);

        }

        return $oProyectoEmprendimiento->idEmprendedor;
    }

    public static function obtenerProyectos()
    {
        $oModelo = new static();

        $sQuery = "SELECT proyectos_emprendimiento.id, proyectos_emprendimiento.idEmprendedor, proyectos_emprendimiento.nombre, CONCAT(emprendedores.nombre,\" \", emprendedores.apellido_p,\" \", emprendedores.apellido_m) AS nombreEmprendedor
                   FROM proyectos_emprendimiento
                   INNER JOIN emprendedores ON proyectos_emprendimiento.idEmprendedor = emprendedores.id
                   WHERE proyectos_emprendimiento.`ON` = 1
                    ";

        if(!$mProyectos = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        return $mProyectos;
    }

    public static function eliminarProyecto($nIdProyecto)
    {
        if(!$nIdProyecto)
        {
            return array('success' => false, 'msg' => 'No se especificó el proyecto');
        }

        if(!$oProyecto = self::findById($nIdProyecto))
        {
            return array('success' => false, 'msg' => 'Proyecto no encontrado en la base de datos');
        }

        if(!empty($oProyecto)) /*Ve si esta vacia*/
        {
            $oProyecto->ON = 0;
            $oProyecto->save();
        }
        return $oProyecto->idEmprendedor;
    }


}