<?php

class AreasConocimiento extends ModelBase
{
    protected $sTable = 'areas_conocimiento';
    protected $sPrimaryKey = 'id';


    public static function obtenerAreasConocimiento()
    {
        if (!$mAreasConocimiento = self::where(array("`ON` = '1'"), array('id', 'nombre'))) {
            return false;
        }
        return $mAreasConocimiento;
    }

    public static function obtenerInfoAreaConocimiento($nIdAreaConocimiento)
    {
        $oModelo = new static();

        $sQuery= "SELECT areas_conocimiento.id,nombre
        FROM areas_conocimiento
        WHERE areas_conocimiento.id = '$nIdAreaConocimiento' ";

        /*trae las funciones d ela BD*/
        if(!$mInfoAreaConocimiento = $oModelo->_db->getOne($sQuery))
        {
            return false;
        }
            return $mInfoAreaConocimiento;
    }
}

