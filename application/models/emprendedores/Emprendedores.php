<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 09/11/17
 * Time: 13:42
 */

class Emprendedores extends ModelBase
{
    protected $sTable = "emprendedores";
    protected $sPrimaryKey = "id";

    public static function obtenerEmprendedores()
    {
        $oModelo = new static();

        $sQuery= "SELECT emprendedores.id, CONCAT(emprendedores.nombre,\" \", emprendedores.apellido_p,\" \", emprendedores.apellido_m) AS nombre, emprendedores.correo, emprendedores.telefono
                  FROM emprendedores
                  WHERE emprendedores.`ON` = 1";

        if(!$mEmprendedores = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }
        return $mEmprendedores;
    }

    public static function obtenerEmprendedor($nIdEmprendedor)
    {
        $oModelo = new static();

        $sQuery= "SELECT usu_usuarios.id as idUsuario, proyectos_emprendimiento.estado, proyectos_emprendimiento.pais, proyectos_emprendimiento.ciudad, emprendedores.id, emprendedores.nombre, emprendedores.apellido_p, emprendedores.apellido_m, CONCAT(emprendedores.nombre,\" \", emprendedores.apellido_p,\" \", emprendedores.apellido_m) AS nombreEmprendedor, emprendedores.nacionalidad, emprendedores.correo, emprendedores.telefono, proyectos_emprendimiento.nombre as nombreProyecto, proyectos_emprendimiento.bEmpresaConstituida, proyectos_emprendimiento.correo_contacto, proyectos_emprendimiento.nombre_integrantes, proyectos_emprendimiento.descripcion_producto, proyectos_emprendimiento.descripcion_base, usu_usuarios.`password`
                  FROM emprendedores
                  INNER JOIN usu_usuarios ON usu_usuarios.idEmprendedor = emprendedores.id
                  INNER JOIN proyectos_emprendimiento ON proyectos_emprendimiento.idEmprendedor = emprendedores.id
                  WHERE emprendedores.`ON` = 1 AND emprendedores.id = '{$nIdEmprendedor}'";

        if(!$mEmprendedores = $oModelo->_db->getOne($sQuery))
        {
            return false;
        }
        return $mEmprendedores;
    }

    public static function agregarEmprendedor($aEmprendedor)
    {
        if($aEmprendedor['id'])
        {
            if(!$oEmprendedor = Emprendedores::findById($aEmprendedor['id']))
            {
                return false;
            }

            $oEmprendedor->update($aEmprendedor);
        }else{

            $oEmprendedor = self::create($aEmprendedor);

        }

        return $oEmprendedor->id;
    }

    public static function eliminarEmprendedor($nIdEmprendedor)
    {
        if(!$nIdEmprendedor)
        {
            return array('success' => false, 'msg' => 'No se especificó el emprendedor');
        }

        if(!$oEmprendedor = self::findById($nIdEmprendedor))
        {
            return array('success' => false, 'msg' => 'Emprendedor no encontrado en la base de datos');
        }

        if(!empty($oEmprendedor))
        {
            $oEmprendedor->ON = 0;
            $oEmprendedor->save();
        }
        return array ('success' => true);
    }

}