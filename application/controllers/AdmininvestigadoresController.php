<?php

require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'usuarios/UsuUsuarios.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';

class AdmininvestigadoresController extends ControllerBase{

    public function init()
    {
        Autentificar::validarLogin();
    }

    public function goIndex()
    {
        //Barra de navegación
        $aNavegacion = array(
            'Bienvenida'
        );

        $this->_view->showSistemaMain('admin/investigadores/index.php', compact('aNavegacion'));
    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            'Nuevo investigador'
        );

        $this->_view->showSistemaMain('admin/investigadores/formulario.php', compact('aNavegacion'));
    }

    public function doAgregar()
    {
        $oResponse = new ResponseForm();

        //formulario
        $aForm = $this->_request['form'];

        //Si el investigador ya existe se agrega un error
        if(Investigadores::where(array("`ON` = 1 AND correo_personal = '{$aForm['correo_personal']}'")))
        {
            $oResponse->addErrorMensaje('Error ya existe un Investigador registrado con el mismo correo');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            //Item activo del menú
            $activeItem = 'inicio';

            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Agregar video"
            );


            $this->_view->showSistemaMain('investigadores/formulario.php', compact('oErrors', 'activeItem', 'aNavegacion', 'aSubAreasConocimiento'));
            return;

        }

        $aForm['fecha_nac'] = Utils::formatDateToDatabase($aForm['fecha_nac'], 'dd/mm/yyyy');
        $aForm['id_investigador'] = Session::get('idInvestigador');

        //registrar nuevo investigador
        if($nIdInvestigador = Investigadores::agregarInvestigador($aForm))
        {

            $aDatosUsuario = array(
                'idInvestigador' => $nIdInvestigador,
                'idPerfil' => 2,
                'nombre' => $aForm['nombre'],
                'email' => $aForm['correo_personal'],
                'password' => $aForm['password']
            );


            $oUsuario = UsuUsuarios::agregarUsuario($aDatosUsuario);

            ResponseForm::addFlashNotice("Se ha agregado el investigador correctamente");//Mensaje de subido

            $this->_redirect($this->_config->get('baseUrl') . 'admin/investigadores/listado');
        }
    }

    public function goEditar()
    {
        $nIdInvestigador = $this->_request['investigador'];
        //Barra de navegación
        $aNavegacion= array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            "Actualizar Investigador"
        );

        $bActualizar = '1';/*boleano*/
        $aInvestigador = Investigadores::obtenerInvestigador($nIdInvestigador); /*Tiene los datos de investigador de la bd*/

        $this->_view->showSistemaMain('admin/investigadores/formulario.php', compact('aNavegacion','bActualizar','aInvestigador'));
    }

    public function doEditar()
    {
        $nIdInvestigador = $this->_request['investigador'];
        $aForm = $this->_request['form'];
        $aForm['id'] = $nIdInvestigador;

        if($nIdInvestigador = Investigadores::agregarInvestigador($aForm))
        {
            $aUsuario = array(
                'ID' => $nIdInvestigador,
                'email' => $aForm['correo_personal'],
            );

            if($aForm['password'])
            {
                $aUsuario['password'] = $aForm['password'];
            }

            UsuUsuarios::agregarUsuario($aUsuario);

            ResponseForm::addFlashNotice('Se ha editado el investigador correctamente');

            $this->_redirect($this->_config->get('baseUrl') . 'admin/investigadores/listado');
        }
    }

    public function goMisDatos()
    {
        $bActualizar = '1';

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            'Mis datos'
        );

        $aInvestigador = Investigadores::obtenerInvestigador(Session::get('idInvestigador'));


        $this->_view->showSistemaMain('admin/investigadores/misDatos.php', compact('aNavegacion', 'aInvestigador', 'bActualizar'));
    }

    public function doActualizarMisDatos()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = Session::get('idInvestigador');

        if($aForm['id'] = Investigadores::agregarInvestigador($aForm))
        {

            $aUsuario = array(
                'ID' => Session::get('idUsuario'),
                'idInvestigador' => $aForm['id'],
                'email' => $aForm['correo_personal']
            );

            if($aForm['password'])
            {
                $aUsuario['password'] = $aForm['password'];
            }

            UsuUsuarios::agregarUsuario($aUsuario);

            ResponseForm::addFlashNotice('Se han guardado tus datos correctamente');
            $this->_redirect($this->_config->get('baseUrl') . 'investigador/misdatos');
        }
    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            'Administrar Investigadores'
        );

        $this->_view->showSistemaMain('admin/investigadores/listado.php', compact('aNavegacion'));
    }

    public function obtenerJson()
    {
        $aInvestigadores = Investigadores::obtenerInvestigadores();

        $this->_view->showJson(array('data' => $aInvestigadores));
    }

    public function ajaxEliminar()
    {
        $nIdInvestigador = $this->_request['investigador'];

        $aUsuario = UsuUsuarios::where(array("usu_usuarios.`ON` = '1' AND usu_usuarios.idInvestigador = {$nIdInvestigador}"));

        //Se desactiva el investigador así como su usuario
        if($nIdUsuario = Investigadores::eliminarInvestigador($nIdInvestigador))
        {
            $aResult = UsuUsuarios::eliminarUsuario($aUsuario[0]['ID']);
        }

        $this->_view->showJson($aResult);
    }



}