<?php

require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';

class AreasconocimientoController extends ControllerBase
{
    public function init()
    {

    }

    public function goArea()
    {
        /*obtener el id del area del conocimiento*/
        /*$data['nIdAreaConocimiento'] = $this->_request['id'];*/

        /*Obtiene la ifo del Area del conocimiento que esta en otra ventana*/
        $data['aInfoAreaConocimiento'] = AreasConocimiento::obtenerInfoAreaConocimiento($this->_request['id']);

        /*mostrar el HTML*/
        $this->_view->showMain('areasConocimiento.php', $data);

    }

}