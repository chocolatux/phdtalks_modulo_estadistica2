<?php
/**
 * Clase AdminController
 * 
 * La clase manipula las acciones del index
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Controllers
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';

class AdminController extends ControllerBase {

    public function init()
    {
        Autentificar::validarLogin();
    }

    public function goModulo()
    {
        $this->_view->show('YoutubeUploader/web-server/index.php');
    }

    public function doEditar()
    {

    }

    public function goListado()
    {
        $this->_view->showSistemaMain('admin/usuarios/listado.php');
    }

}