<?php
/**
 * Clase VideosController
 *
 * La clase manipula las acciones de los videos del sistema
 *
 * Creado 11/Abril/2017
 *
 * @category Class
 * @package Controllers
 * @author Emmanuel
 */

require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';
require_once $config->get('modelsFolder') . 'registroVisitas/RegistroVisitas.php';


class VideosController extends ControllerBase {

    public function init()
    {

    }

    private function contadorVisitas($nCantidadVisitas, $nIdVideo, $nClicsArticulo)
    {
        $nCantidadVisitas += 1;

        $aVideo = array(
            'id' => $nIdVideo,
            'visitas' => $nCantidadVisitas
        );

        $aRegistro = array(
            'id_video' => $nIdVideo,
            'fecha_hora' => date('Y-m-d H:i:s'),
            //La variable tipo hace referencia al tipo de registro 1 para videos y 2 para articulos
            'tipo' => 1
        );

        if(isset($nClicsArticulo))
        {
            $nClicsArticulo += 1;
            $aVideo['clics_articulo'] = $nClicsArticulo;
            //La variable tipo hace referencia al tipo de registro 1 para videos y 2 para articulos
            $aRegistro['tipo'] = 2;
        }

        Videos::agregarVideo($aVideo);

        if($nIdUsuario = Session::get('idUsuario'))
        {
            $aRegistro['id_usuario'] = $nIdUsuario;
        }

        //Se crea el registro de la visita
        RegistroVisitas::agregarRegistro($aRegistro);
    }

    public function goArticulo()
    {
        $nIdVideo = $this->_request['id'];
        $aVideo = Videos::obtenerInfoVideo($nIdVideo);
        $this->contadorVisitas($aVideo['visitas'], $nIdVideo, $aVideo['clics_articulo']);
        $this->_redirect($aVideo['liga_articulo']);

    }

    public function goVisualizar()
    {
        $nIdVideo = $this->_request['id'];

        //Obtener datos del video a analizar
        $data['aInfoVideo'] = Videos::obtenerInfoVideo($nIdVideo);

        //Contador de visitas
        $this->contadorVisitas($data['aInfoVideo']['visitas'], $nIdVideo);

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();
        $this->_view->showMain('videos/visualizacion.php', $data);
    }

    public function goVideosArea()
    {
        $nIdArea = $this->_request['id'];

        //Obtener videos
        $data['aVideosAreas'] = Videos::obtenerVideosPorArea($nIdArea);

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        foreach($data['areasConocimiento'] as $aAreasConocimiento)
        {
            if($nIdArea == $aAreasConocimiento['id'])
            {
                $data['sAreaConocimiento'] = $aAreasConocimiento['nombre'];
            }
        }

        $this->_view->showMain('videos/videosPorArea.php', $data);

    }

    public function goBuscar()
    {
        //se obtiene las palabras "r" es la variable que pasa 'buscar?r=" '
        $sPalabra = $this->_request['r'];

        //Se obtienen las categorias para el menú
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        //Se obtienen los videos que coincidan con la palabra
        $data['busqueda'] = Videos::buscarVideo($sPalabra);
        //$data['busqueda']['areas'] = Videos::obtenerInfoVideo($data['busqueda']['videos']);
        $data['busqueda']['palabra'] = $sPalabra;

        $this->_view->showMain('videos/buscar.php', $data);
    }

    public function ajaxObtenerVideos()
    {
        //La cantidad de videos que se desea mostrar por bloque
        $nCantidadVideos = 9;

        //Se obtiene el número del primer video
        $nPaginaActual = $this->_request['p'];

        $nVideoInicial = ($nPaginaActual * $nCantidadVideos);

        $aVideosRecientes = Videos::obtenerVideosRecientes($nVideoInicial);

        $this->_view->showJson($aVideosRecientes);
    }

}

