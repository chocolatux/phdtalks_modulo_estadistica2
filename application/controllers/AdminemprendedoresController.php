<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 11/11/17
 * Time: 13:46
 */
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'emprendedores/Emprendedores.php';
require_once $config->get('modelsFolder') . 'proyectosEmprendimiento/ProyectosEmprendimiento.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';


class AdminemprendedoresController extends ControllerBase
{
    public function init()
    {
        Autentificar::validarLogin();
    }

    public function goIndex()
    {
        //Barra de navegación
        $aNavegacion = array(
            'Bienvenida'
        );

        $this->_view->showSistemaMain('admin/emprendedores/index.php', compact('aNavegacion'));
    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Nuevo emprendedor'
        );

        $this->_view->showSistemaMain('admin/emprendedores/formulario.php', compact('aNavegacion'));
    }

    public function doAgregar()
    {
        $oResponse = new ResponseForm();

        //formulario
        $aForm = $this->_request['form'];

        //Si el emprendedor ya existe se agrega un error
        if(Emprendedores::where(array("`ON` = 1 AND correo = '{$aForm['correo']}'")))
        {
            $oResponse->addErrorMensaje('Error ya existe un emprendedor registrado con el mismo correo');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            $this->_view->showMain('registroEmprendedor.php', compact('oErrors', 'aSubAreasConocimiento'));

            return;
        }

        //registrar nuevo investigador
        if($nIdEmprendedor = Emprendedores::agregarEmprendedor($aForm))
        {

            $aDatosUsuario = array(
                'idEmprendedor' => $nIdEmprendedor,
                'idPerfil' => 3,
                'nombre' => $aForm['nombre'],
                'email' => $aForm['correo'],
                'password' => $aForm['password']
            );

            $oUsuario = UsuUsuarios::agregarUsuario($aDatosUsuario);

            $aDatosProyecto = array(
                'idEmprendedor' => $nIdEmprendedor,
                'nombre' => $aForm['nombre_proyecto'],
                'bEmpresaConstituida' => $aForm['bEmpresaConstituida'],
                'pais' => $aForm['pais'],
                'estado' => $aForm['estado'],
                'ciudad' => $aForm['ciudad'],
                'correo_contacto' => $aForm['correo_contacto'],
                'nombre_integrantes' => $aForm['nombre_integrantes'],
                'descripcion_producto' => $aForm['descripcion_producto'],
                'descripcion_base' => $aForm['descripcion_base']
            );

            ProyectosEmprendimiento::agregarProyectoEmprendimiento($aDatosProyecto);

            ResponseForm::addFlashNotice('Tu cuenta ha sido creada exitosamente ingresa tus datos para iniciar sesión');

            $this->_redirect($this->_config->get('baseUrl') . 'login');

        }

    }

    public function goEditar()
    {
        $nIdEmprendedor = $this->_request['emprendedor'];
        //Barra de navegación
        $aNavegacion= array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            "Actualizar Emprendedor"
        );

        $bActualizar = '1';
        $aEmprendedor = Emprendedores::obtenerEmprendedor($nIdEmprendedor);

        $this->_view->showSistemaMain('admin/emprendedores/formulario.php', compact('aNavegacion','bActualizar','aEmprendedor'));

    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];

        $aForm['idEmprendedor'] = $this->_request['emprendedor'];
        $aForm['id'] = $this->_request['proyecto'];

        $aDatosProyecto = array(
            'id' => $this->_request['proyecto'],
            'nombre' => $aForm['nombre_proyecto'],
            'bEmpresaConstituida' => $aForm['bEmpresaConstituida'],
            'pais' => $aForm['pais'],
            'ciudad' => $aForm['ciudad'],
            'estado' => $aForm['estado'],
            'correo_contacto' => $aForm['correo_contacto'],
            'nombre_integrantes' => $aForm['nombre_integrantes'],
            'descripcion_producto' => $aForm['descripcion_producto'],
            'descripcion_base' => $aForm['descripcion_base'],
        );

        $aDatosEmprendedor = Emprendedores::obtenerEmprendedor($aForm['idEmprendedor']);

        if($nIdEmprendedor = ProyectosEmprendimiento::agregarProyectoEmprendimiento($aDatosProyecto))
        {
            $aForm['id'] = $nIdEmprendedor;

            Emprendedores::agregarEmprendedor($aForm);

            $aDatosUsuario = array(
                'ID' => $aDatosEmprendedor['idUsuario'],
                'nombre' => $aForm['nombre'],
                'email' => $aForm['correo']
            );

            if($aForm['password'])
            {
                $aDatosUsuario['password'] = $aForm['password'];
            }

            UsuUsuarios::agregarUsuario($aDatosUsuario);

            ResponseForm::addFlashNotice('Se ha editado el Emprendedor correctamente');

            $this->_redirect($this->_config->get('baseUrl') . 'admin/emprendedores/listado');
        }
    }

    public function goMisDatos()
    {
        $bActualizar = '1';

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'emprendedor/index' => 'Bienvenida',
            'Mis datos'
        );

        $aEmprendedor = Emprendedores::obtenerEmprendedor(Session::get('idEmprendedor'));

        $this->_view->showSistemaMain('admin/emprendedores/misDatos.php', compact('aNavegacion', 'aEmprendedor', 'bActualizar'));
    }

    public function doActualizarMisDatos()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = Session::get('idEmprendedor');

        $aDatosProyecto = array(
            'id' => Session::get('idEmprendedor'),
            'nombre' => $aForm['nombre_proyecto'],
            'bEmpresaConstituida' => $aForm['bEmpresaConstituida'],
            'pais' => $aForm['pais'],
            'ciudad' => $aForm['ciudad'],
            'estado' => $aForm['estado'],
            'correo_contacto' => $aForm['correo_contacto'],
            'nombre_integrantes' => $aForm['nombre_integrantes'],
            'descripcion_producto' => $aForm['descripcion_producto'],
            'descripcion_base' => $aForm['descripcion_base']
        );

        if($aForm['id'] = Emprendedores::agregarEmprendedor($aForm))
        {
            ProyectosEmprendimiento::agregarProyectoEmprendimiento($aDatosProyecto);

            $aUsuario = array(
                'ID' => Session::get('idUsuario'),
                'email' => $aForm['correo']
            );

            if($aForm['password'])
            {
                $aUsuario['password'] = $aForm['password'];
            }

            UsuUsuarios::agregarUsuario($aUsuario);

            ResponseForm::addFlashNotice('Se han guardado tus datos correctamente');
            $this->_redirect($this->_config->get('baseUrl') . 'emprendedor/misdatos');

        }
    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar Investigadores'
        );

        $this->_view->showSistemaMain('admin/emprendedores/listado.php', compact('aNavegacion'));
    }

    public function obtenerJson()
    {
        $aProyectos = ProyectosEmprendimiento::obtenerProyectos();

        $this->_view->showJson(array('data' => $aProyectos));
    }

    public function ajaxEliminar()
    {
        $nIdProyecto = $this->_request['emprendedor'];

        $nIdEmprendedor = ProyectosEmprendimiento::eliminarProyecto($nIdProyecto);

        if($aResult = Emprendedores::eliminarEmprendedor($nIdEmprendedor))
        {
            $aUsuario = UsuUsuarios::where(array("usu_usuarios.`ON` = '1' AND usu_usuarios.idEmprendedor = {$nIdEmprendedor}"));
            UsuUsuarios::eliminarUsuario($aUsuario[0]['ID']);
        }

        $this->_view->showJson($aResult);
    }
}