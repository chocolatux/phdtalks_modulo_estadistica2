<?php

require_once $config->get('modelsFolder') . 'usuarios/UsuUsuarios.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';


class AdminusuariosController extends ControllerBase{

    public function init()
    {
        Autentificar::validarLogin();

    }

    public function goIndex()
    {

        $aNavegacion = array(
            'Bienvenida'
        );

        $this->_view->showSistemaMain('admin/usuarios/index.php', compact('aNavegacion'));

    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
            'Nuevo administrador'
        );

        $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('aNavegacion'));
    }

    public function doAgregar()
    {
        $aForm = $this->_request['form'];
        $aForm['idPerfil'] = '1';

        $oResponse  = new ResponseForm();

        if(UsuUsuarios::where(array("usu_usuarios.email = '{$aForm['email']}' AND usu_usuarios.`ON` = 1")))
        {
            $oResponse->addErrorMensaje('Error ya existe un usuario registrado con el correo: ' . ' ' . $aForm['email']);

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('oErrors'));
            return;

        }

        if($oUsuario = UsuUsuarios::agregarUsuario($aForm))
        {
            ResponseForm::addFlashNotice('El usuario se registró correctamente con el correo: ' . $aForm['email'] );

            $this->_redirect($this->_config->get('baseUrl') . 'admin/usuarios/listado');

        }

    }

    public function goEditar()
    {
        $bActualizar = '1';

        $nIdUsuario = $this->_request['usuario'];

        $aUsuario = UsuUsuarios::obtenerUsuarioInfo($nIdUsuario);

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
            'Actualizar usuario administrador'
        );

        $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('aNavegacion', 'aUsuario', 'bActualizar'));

    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = $this->_request['usuario'];

        die(var_dump($aForm));


        if(!UsuUsuarios::agregarUsuario($aForm))
        {
            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl') .  'admin/index' => 'Bienvenida',
                'Actualizar usuario administrador'
            );

            $oResponse = new ResponseForm();

            $oResponse->addErrorMensaje('Se produjo un error al editar el usuario administrador');

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/formulario.php', compact('oErrors', 'aNavegacion'));

            return;
        }

        ResponseForm::addFlashNotice('Se actualizó el usuario correctamente');

        $this->_redirect($this->_config->get('baseUrl') . 'admin/usuarios/listado');

    }

    public function goMisDatos()
    {
        $bActualizar = '1';

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Mis datos'
        );

        $aUsuario = UsuUsuarios::obtenerUsuarioInfo(Session::get('idUsuario'));

        $this->_view->showSistemaMain('admin/usuarios/misDatos.php', compact('aNavegacion', 'aUsuario', 'bActualizar'));

    }

    public function doActualizarMisDatos()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = Session::get('idUsuario');

        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Mis datos'
        );

        if(!UsuUsuarios::agregarUsuario($aForm))
        {
            $oResponse = new ResponseForm();

            $oResponse->addErrorMensaje('Se ha producido un error al guardar tus datos');

            $oErrors = $oResponse->getErrors();

            $this->_view->showSistemaMain('admin/usuarios/misDatos.php', compact('oErrors', 'aNavegacion'));
            return;

        }

        ResponseForm::addFlashNotice('Se han guardado tus datos correctamente');
        $this->_redirect($this->_config->get('baseUrl') . 'admin/misdatos');

    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar usuarios administradores'
        );

        $this->_view->showSistemaMain('admin/usuarios/listado.php', compact('aNavegacion'));
    }

    public function obtenerJson()
    {
        $aUsuarios = UsuUsuarios::obtenerUsuarios(Session::get('idUsuario'));

        $this->_view->showJson(array('data' => $aUsuarios));

    }

    public function ajaxEliminar()
    {
        $nIdUsuario = $this->_request['usuario'];

        $aResult = UsuUsuarios::eliminarUsuario($nIdUsuario);

        $this->_view->showJson($aResult);

    }

    public function goEstadistica()
    {
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            $this->_config->get('baseUrl') . 'admin/estadistica/resumen' => 'Estadistica',
            'Resumen Global'
        );

        //para que muestre el menu y filtros
        $bEstadistica = 1;

        $activeItem = "resumen Global";

        $this->_view->showSistemaMain('admin/usuarios/estadistica/resumenGlobal.php',compact('aNavegacion','bEstadistica', 'activeItem'));
    }

    public function goAreasConocimiento()
    {
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            $this->_config->get('baseUrl') . 'admin/estadistica/resumen' => 'Estadistica',
            'Áreas del conocimiento'
        );

        //para que muestre el menu y filtros
        $bEstadistica = 1;

        $activeItem = "areas del Conocimiento";

        $this->_view->showSistemaMain('admin/usuarios/estadistica/areasConocimiento.php',compact('aNavegacion','bEstadistica', 'activeItem'));
    }

    public function goInvestigadores()
    {
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            $this->_config->get('baseUrl') . 'admin/estadistica/resumen' => 'Estadistica',
            'Investigadores'
        );

        //para que muestre el menu y filtros
        $bEstadistica = 1;

        $activeItem = "investigadores";

        $this->_view->showSistemaMain('admin/usuarios/estadistica/investigadores.php',compact('aNavegacion','bEstadistica', 'activeItem'));
    }

    public function goUrlpersonalizados()
    {
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            $this->_config->get('baseUrl') . 'admin/estadistica/resumen' => 'Estadistica',
            'URLs Personalizados'
        );

        //para que muestre el menu y filtros
        $bEstadistica = 1;

        $activeItem = "Urlpersonalizados";

        $this->_view->showSistemaMain('admin/usuarios/estadistica/urlsPersonalizados.php',compact('aNavegacion','bEstadistica', 'activeItem'));
    }

    public function goVideos()
    {
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            $this->_config->get('baseUrl') . 'admin/estadistica/resumen' => 'Estadistica',
            'Videos'
        );

        //para que muestre el menu y filtros
        $bEstadistica = 1;

        $activeItem = "videos";

        $this->_view->showSistemaMain('admin/usuarios/estadistica/videos.php',compact('aNavegacion', 'bEstadistica', 'activeItem'));
    }


}