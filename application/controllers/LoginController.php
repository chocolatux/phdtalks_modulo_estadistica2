<?php
/**
 * Clase LoginController
 * 
 * La clase manipula las acciones del login del sistema
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Controllers
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('modelsFolder').'usuarios/UsuUsuarios.php';
require_once $config->get('utilsFolder').'ResponseForm.php';

class LoginController extends ControllerBase {
    
    /**
     * 
     */
    public function init()
    {
        
    }
    
    /**
     * Acción para loguear al usuario
     * 
     * @return boolean
     */
    public function doLogin()
    {
        //Se valida si tiene session
        if(Autentificar::tieneLogin()) {
            $this->_redirect($this->_config->get('baseUrl'));
        }

        $aForm = $this->_request['form'];

        $email = htmlentities($aForm['email']);
        $password = htmlentities($aForm['password']);

        $oResponse = new ResponseForm($aForm, ['email','password']); //
        
        //Validaciones de form
        if($aForm['email'] == '') {
            
            $oResponse->addErrorForm('email', 'El usuario no es válido');
        }
        
        if($aForm['password'] == '') {
            
            $oResponse->addErrorForm('password', 'La contraseña no es válida');
        }
        
        if($oResponse->anyError()) {
            
            $oResponse->addErrorMensaje('Usuario o contraseña no válida');
            
            $oErrors = $oResponse->getErrors();
            $oForm = $oResponse->getForm();
            
            $sSectionFile = 'login';
            
            $this->_view->showLogin($sSectionFile, compact('sSectionFile', 'oErrors', 'oForm'));
            return true;
        }
        
        //Validar Datos
        if(!$oUsuUsuario = Autentificar::login($aForm['email'], $aForm['password']) ) {
            
            $oResponse->addErrorMensaje('Usuario o contraseña no válida');
            
            $oErrors = $oResponse->getErrors();
            $oForm = $oResponse->getForm();

            $this->_view->showLogin('login.php', compact('sSectionFile', 'oErrors'));

            return false;

        }

        //Si el usuario es administrador
        if((Session::get('idPerfil')) == '1'){
            $this->_redirect($this->_config->get('baseUrl') . 'admin/index');
        }

        //Si el usuario es emprendedor
        if((Session::get('idPerfil')) == '3'){
            $this->_redirect($this->_config->get('baseUrl') . 'emprendedor/index');
        }

        $this->_redirect($this->_config->get('baseUrl') . 'investigador/index');
    }
    
    /**
     * Ir a la pantalla de login
     */
    public function goLogin()
    {
        //Se valida si tiene session
        if(Autentificar::tieneLogin()) {
            $this->_redirect($this->_config->get('baseUrl'));
        }
        
        $this->_view->showLogin('login.php');
    }
    
    /**
     * Acción para terminar la sessión del usuario actual
     */
    public function goLogout()
    {
        Autentificar::logout();
        
        $this->_redirect($this->_config->get('baseUrl'));
    }
    
}