<?php
/**
 * Clase Response
 * 
 * Auxiliar para la respueste regresada al usuario
 * 
 * Creado 09/Mayo/2016
 * 
 * @category Class
 * @package Utils
 * @author David Heredia <david@chocolatux.com>
 */
class ResponseForm {
    
    /**
     *
     * @var stdClass 
     */
    private $_oErrors = null;
    
    /**
     *
     * @var boolean 
     */
    private $_bErrors = false;
    
    /**
     *
     * @var stdClass
     */
    private $_oForm = null;
    
    /**
     *
     * @var Config
     */
    private $_config = null;
    
    /**
     *
     * @var array 
     */
    private $_aCamposExcluidos = array();
    
    /**
     * 
     * @param array $aDatosForm
     * @param array $aCamposExcluidos
     */
    function __construct($aDatosForm, $aCamposExcluidos)
    {
        $this->_config = Config::getInstance();
        
        $this->_oErrors = new stdClass();
        $this->_oErrors->form = array();
        $this->_oErrors->mensajes = array();
        
        $this->_oForm = array();
        
        $this->_aCamposExcluidos = $aCamposExcluidos;
        
        //$this->setErrors($aDatosForm);
        $this->setForm($aDatosForm);
    }

    /**
     * 
     * @param string $sCampo
     * @param string $sMensaje
     */
    public function addErrorForm ($sCampo, $sMensaje = '', $sSubindice='')
    {
        if($sSubindice)
        {
            $this->_oErrors->form[$sCampo][$sSubindice] = $sMensaje;
        }
        else 
        {
            $this->_oErrors->form[$sCampo] = $sMensaje;
        }
        $this->_bErrors = true;
    }
    
    /**
     * 
     * @param string $sMensaje
     */
    public function addErrorMensaje ($sMensaje)
    {
        $this->_oErrors->mensajes[] = $sMensaje;
        $this->_bErrors = true;
    }
    
    /**
     * 
     * @return boolean
     */
    public function anyError ()
    {
        return $this->_bErrors;
    }
    
    /**
     * 
     * @return stdClass
     */
    public function getErrors()
    {
        return $this->_oErrors;
    }
    /**
     *
     * @return stdClass
     */
    public function getErrorsForm()
    {
        return count($this->_oErrors->form) ? true : false;
    }

    /**
     * 
     * @return stdClass
     */
    public function getForm()
    {
        return $this->_oForm;
    }
    
    /**
     * Agrega un mensaje tipo error al flashMessenger
     * 
     * @param string $sMensaje
     */
    public static function addFlashError($sMensaje)
    {
        $oInstancia = new static;
        $oInstancia->_config->get('flashMessenger')->addMessage('mensajesError', $sMensaje);
    }
    
    /**
     * Agrega un mensaje tipo error al flashMessenger
     * 
     * @param string $sMensaje
     */
    public static function addFlashNotice($sMensaje)
    {
        $oInstancia = new static;
        $oInstancia->_config->get('flashMessenger')->addMessage('mensajesNotice', $sMensaje);
    }


    /**
     * 
     * @param array $aForm
     */
    private function setErrors($aForm)
    {
        foreach ($aForm as $sCampo => $sValor) {
            
                $this->_oErrors->form[$sCampo] = NULL;
        }
    }
    
    /**
     * 
     * @param array $aForm
     */
    private function setForm($aForm)
    {
        foreach ($aForm as $sCampo => $sValor) {
                
            if( !in_array($sCampo, $this->_aCamposExcluidos) )
            {
                $this->_oForm[$sCampo] = $sValor;
            }
        }
    }
}
