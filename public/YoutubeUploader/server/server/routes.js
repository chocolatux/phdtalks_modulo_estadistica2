/**
 * Main app routes
 */
'use strict';


const request = require('request');

/*
const options = {
    url: 'http://phdtalks-bare.local/',
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
    }
};
 */

var $ = null;
var jsdom = require('jsdom/lib/old-api');
var express = require('express');
var runner = require('child_process');
var fs = require('fs');
var multer = require('multer');
var path = require('path');
var bodyParser = require('body-parser');


//FFMPEG
var ffmpeg = require('fluent-ffmpeg');
var command = ffmpeg();

module.exports = function(app) {

  //Routes for the API
  var router = express.Router();

  router.use(function(req, res, next) {
    // Necessary in order to receive and write files
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    next();
  });

  router.use(bodyParser.urlencoded({extended: false}));
  router.use(bodyParser.json());

  //general routes
  router.get('/', function(req, res) {
    res.json({
      microservice: 'taks-microservice',
      owner: 'PhDTalks'
    });
  });

  router.post('/prueba', function(req, res){
      var post_body = req.body;
      console.log('req bien chingon: ', post_body);
      res.status(200).end('File is uploaded!');
    });

  // Server route /file is marked as the one in charge of receive the file
  router.post('/file', function(req, res) {

      var upload = multer({
      storage: multer.memoryStorage()
    }).single('userVideo');
    upload(req, res, function(err) {

        //console.log(req);

      var buffer = req.file.buffer;
      var type = (req.file.originalname.split("."))[1];

      // Metadata is received and then a file is written within
      var filename = req.file.fieldname + '-' + Date.now() + path.extname(req.file.originalname) + ".mp4";
      fs.writeFile('./uploads/' + filename, buffer, 'binary', function(err) {
        if(err){
          res.status(500).end("File couldn't be saved");
        }else {
          console.log("Save file was a success!");
          getVideoSize("./uploads/" + filename, handleConcatVideos);
        }
      });


     fs.writeFile("userVideoDataTemp.txt", req, function(err) {
     if(err) {
        return console.log(err);
     }

     });


      /*
      request(options, function(err, res, body){

        var json = [{
          holi: 'holi'
        }];


          var b = JSON.parse(JSON.stringify(json));
          res.status(200).end('file is uploaded');
      });*/



      /*
        fs.writeFile("userVideoDataTemp.txt", jsonData, function(err) {
            if(err) {
                return console.log(err);
            }
        });*/

      res.status(200).end('File is uploaded!');
    });

  });

  function handleConcatVideos(pathToVideoRecorded, sizes){

    // Path to videos with different resolutions
    var pathTo480 = 'PhD_Talks_Intro_480.mp4';
    var pathTo720 = 'PhD_Talks_Intro_720.mp4';
    var pathTo1080 = 'PhD_Talks_Intro_1080.mp4';

    var introToUse = null;

    var width = sizes.width;
    var height = sizes.height;

    if(width === 1920 && height === 1080){
      introToUse = pathTo1080;
    }
    else if(width === 1280 && height === 720){
      introToUse = pathTo720;
    }
    else if(width === 640 && height === 480){
      introToUse = pathTo480;
    }

    if(introToUse !== null){
      console.log("Video has correct size");
      var newFileName = 'TestingMerge' + '-' + Date.now() + '.mp4';
      concatVideos(introToUse, pathToVideoRecorded, newFileName);
    }
    else{
      return "Video recorded has no appropiate size (resolution)";
    }
    
  }

  // Both videos's size need to match, so this function retrieves the size
  // reading a file is an async method
  function getVideoSize(pathToVideo, callbackFunction){
    console.log("\nGetting size of recorded video");

    ffmpeg(pathToVideo)
      .ffprobe(0, function(err, data) {
        if(err){
          console.log("Error while obtaining sizes");
          console.log(err);
          return false;
        }

        var sizes = {
          width: data.streams[0].width,
          height: data.streams[0].height,
        };

        console.dir(sizes);
        callbackFunction(pathToVideo, sizes);
      }
    );
  }

  function getWidth(pathToVideo){
    ffmpeg(pathToVideo)
      .ffprobe(0, function(err, data) {
        if(err){
          console.log("Error obtaining Width");
          console.log(err);
          return false;
        }

        console.dir("Width: " + data.streams[0].width);
        return data.streams[0].width;
      }
    );
  }

  function getHeight(pathToVideo){
    ffmpeg(pathToVideo)
      .ffprobe(0, function(err, data) {
        if(err){
          console.log("Error obtaining Height");
          console.log(err);
          return false;
        }
        console.dir("Height: " + data.streams[0].height);
        return data.streams[0].height;
      }
    );
  }

  function concatVideos(introVideo, recordedVideo, newFileName){

    console.log("Merging video");
    console.log(introVideo);
    console.log(recordedVideo);
    console.log(newFileName);

    var app = express();
    var tempPath = '/tempFiles/';
    var phpScriptPath = 'upload.php';
    var argsString = newFileName;

    newFileName = "./merged/" + newFileName;

    ffmpeg()
    // Videos (order matters) that are going to be added
      .input(introVideo)
      .input(recordedVideo)
      .input(introVideo)
        .on('error', function(err) {
      console.log('An error occurred: ' + err.message);
    })
      .on('end', function(){
          console.log('intentando ejecutar un sript php');
          runner.exec('php ' + phpScriptPath + ' ' +argsString, function(err, phpResponse, stderr){
              if(err) console.log(err);
              console.log(phpResponse);

              jsdom.env(
                  'http://phdtalks-bare.local',
                  function(err, window) {
                      $ = require('jquery')(window);

                      $.post('http://phdtalks-bare.local/response',{response: phpResponse},function(data){
                          console.log('me quiero volver chango ' + data);
                      });
                  }
              );
          });
      })
      .concat(newFileName, tempPath);
  }

  // All routes use "/api/" in their path. So /file would be /api/file
  app.use('/api', router);
};
