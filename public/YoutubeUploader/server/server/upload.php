<?php

//include api config file
require_once 'config.php';

$key = file_get_contents('the_key.txt');
$client->setAccessToken($key);

// Check to ensure that the access token was successfully acquired.
if ($client->getAccessToken()) {
    $htmlBody = '';
    try{

        if($client->isAccessTokenExpired())
        {
            $newToken = json_decode($client->getAccessToken());
            if(!$newToken->refresh_token)
            {
                echo 'Error al renovar el token, asegurese de que tenga habilitada esta opción en su cuenta';
            }
            $client->refreshToken($newToken->refresh_token);
            file_put_contents('the_key.txt', $client->getAccessToken());
        }

        $sNameVideo = $argv[1];
        // REPLACE this value with the path to the file you are uploading.
        $videoPath = 'merged/' . $sNameVideo;
        $htmlBody .= 'el video path es: '. $videoPath;

        // uploaded video data
        $videoTitle = $sNameVideo;
        $videoDesc = "otro video de prueba";
        $videoTags = array("youtube22", "tutorial22");

        // Create a snippet with title, description, tags and category ID
        // Create an asset resource and set its snippet metadata and type.
        // This example sets the video's title, description, keyword tags, and
        // video category.
        $snippet = new Google_Service_YouTube_VideoSnippet();
        $snippet->setTitle($videoTitle);
        $snippet->setDescription($videoDesc);
        $snippet->setTags($videoTags);

        // Numeric video category. See
        // https://developers.google.com/youtube/v3/docs/videoCategories/list
        $snippet->setCategoryId("22");

        // Set the video's status to "public". Valid statuses are "public",
        // "private" and "unlisted".
        $status = new Google_Service_YouTube_VideoStatus();
        $status->privacyStatus = "public";

        // Associate the snippet and status objects with a new video resource.
        $video = new Google_Service_YouTube_Video();
        $video->setSnippet($snippet);
        $video->setStatus($status);

        // Specify the size of each chunk of data, in bytes. Set a higher value for
        // reliable connection as fewer chunks lead to faster uploads. Set a lower
        // value for better recovery on less reliable connections.
        $chunkSizeBytes = 1 * 1024 * 1024;

        // Setting the defer flag to true tells the client to return a request which can be called
        // with ->execute(); instead of making the API call immediately.
        $client->setDefer(true);

        // Create a request for the API's videos.insert method to create and upload the video.
        $insertRequest = $youtube->videos->insert("status,snippet", $video);

        // Create a MediaFileUpload object for resumable uploads.
        $media = new Google_Http_MediaFileUpload(
            $client,
            $insertRequest,
            'video/*',
            null,
            true,
            $chunkSizeBytes
        );
        $media->setFileSize(filesize($videoPath));


        // Read the media file and upload it chunk by chunk.
        $status = false;
        $handle = fopen($videoPath, "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }

        fclose($handle);

        // If you want to make other calls after the file upload, set setDefer back to false
        $client->setDefer(false);

        //delete video file from local server
        @unlink("merged/" . $sNameVideo);

        // uploaded video data
        $videoTitle = $status['snippet']['title'];
        $videoDesc = $status['snippet']['description'];
        $videoTags = $status['snippet']['tags'];
        $videoId = $status['id'];

        //echo uploaded video ID
        echo $videoId;

        /*
        $htmlBody .= "<p class='succ-msg'>Video Uploaded to YouTube</p>";
        $htmlBody .= '<embed width="400" height="315" src="https://www.youtube.com/embed/'.$videoId.'"></embed>';
        $htmlBody .= '<ul><li><b>Title: </b>'.$videoTitle.'</li>';
        $htmlBody .= '<li><b>Description: </b>'.$videoDesc.'</li>';
        $htmlBody .= '<li><b>Tags: </b>'.$videoTags.'</li></ul>';
        $htmlBody .= '<a href="logout.php">Logout</a>';*/

    } catch (Google_Service_Exception $e) {
        $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    } catch (Google_Exception $e) {
        $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
        $htmlBody .= 'Please reset session <a href="logout.php">Logout</a>';
    }

} elseif ($OAUTH2_CLIENT_ID == '96984430772-gn1ir5u16jrlfvfspgcoo445gh7km4n5.apps.googleusercontent.com') {

    echo 'Client credentials required';

} else {
    // If the user hasn't authorized the app, initiate the OAuth flow
    $state = mt_rand();
    $client->setState($state);
    $_SESSION['state'] = $state;

    $authUrl = $client->createAuthUrl();
    echo 'Authorization Required ' . $authUrl . ' authorize access before proceeding.';
}


