var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
var mediaRecorder;
var recordedBlobs;
var sourceBuffer;

// Initialize web elements
var gumVideo      = document.querySelector('video#camaraWeb');
var recordedVideo = document.querySelector('video#videoGrabado');

var recordButton  = document.querySelector('button#record');
var playButton    = document.querySelector('button#play');
//var downloadButton = document.querySelector('button#download');
var uploadButton  = document.querySelector('button#sendToServer');
var restartRecord = document.querySelector('button#volverGrabar');

//Variables para reloj
var time = 180;
var initialOffset = '706';
var s = 0; /*segundos con los que camina la animación*/
var seg = 1;
var m = 0;
//var btngraba = 0;
var interval;

const txt1 = document.getElementById("txt1");
const txt2 = document.getElementById("txt2");
const txt3 = document.getElementById("txt3");
const txt4 = document.getElementById("txt4");
const fechaTituloArticulo = document.getElementById("fechaTituloArticulo");
const flechaAplicarConocimiento = document.getElementById("flechaAplicarConocimiento");
const fechaResultadoArticulo = document.getElementById("fechaResultadoArticulo");
const fechaObjetivoArticulo = document.getElementById("fechaObjetivoArticulo");
const btnIcon2 = document.getElementById("btnIcon2");
const btnIconNombre = document.getElementById("btnIconNombre");
const arbolSeleccion = document.getElementById("arbolSeleccion");
const record = document.getElementById("record");

console.log('titulo ' + inputTitulo);
//Constante texto del tiempo
const txtTiempo = document.getElementById("txtTiempo");

/* Need initial run as interval hasn't yet occured... */
$('.circle_animation').css('stroke-dashoffset', initialOffset-(1*(initialOffset/time)));

// Setup onclick methods (this can also be added by onclick directly on html)
recordButton.onclick = toggleRecording;
playButton.onclick = play;
//downloadButton.onclick = download;
uploadButton.onclick = uploadServer;
restartRecord.onclick = restartRecording;

function restartRecording()
{
    if(arbolSeleccion.style.visibility = 'visible')
    {
        arbolSeleccion.style.visibility = 'hidden';
        playButton.style.display = 'none';
        recordedVideo.style.display = 'none';
        gumVideo.style.display = 'inline-block';
        recordButton.style.display = 'inline-block';
        s = 0, m = 0; //pendiente
        movimientoRelojAnimacion(s);
        relojTime(m,s);
        flechaCambia(s);
    }

}

function flechaCambia(segundos){
    if(segundos<=20){
        flechaAplicarConocimiento.classList.remove("txtNaranja");
        fechaObjetivoArticulo.classList.remove("txtNaranja");
        fechaResultadoArticulo.classList.remove("txtNaranja");
        txt4.classList.remove("txtActivado");
        txt3.classList.remove("txtActivado");
        txt2.classList.remove("txtActivado");
        fechaTituloArticulo.classList.add("txtNaranja");
        txt1.classList.add("txtActivado");
    }

    if(segundos>=20 && segundos<=60){
        fechaObjetivoArticulo.classList.add("txtNaranja");
        fechaTituloArticulo.classList.remove("txtNaranja");
        txt2.classList.add("txtActivado");
        txt1.classList.remove("txtActivado");
    }

    if(segundos>=60 && segundos<=120){
        fechaResultadoArticulo.classList.add("txtNaranja");
        fechaObjetivoArticulo.classList.remove("txtNaranja");
        txt3.classList.add("txtActivado");
        txt2.classList.remove("txtActivado");
    }

    if(segundos>=120 && segundos<=time){
        flechaAplicarConocimiento.classList.add("txtNaranja");
        fechaResultadoArticulo.classList.remove("txtNaranja");
        txt4.classList.add("txtActivado");
        txt3.classList.remove("txtActivado");
    }
}

function stopTimer()
{
    /*btnIcon.className = "play icon";*/
    btnIconNombre.innerHTML = "Reproducir mi video";
    txtTiempo.innerHTML = "Duración total:";
    arbolSeleccion.style.visibility = "visible";
    btnIcon2.style.display = "none";
    //
    console.log("StopTimer");
    record.style.display = "none";
    clearTimeout(interval);
}

$("#volverGrabar").click(function(){
    arbolSeleccion.style.visibility = "hidden";
    recordButton.innerText ='Iniciar grabación';
    btnIcon2.className = "circle icon" ;
    btnIcon2.style.display = "block";
    /**/
});

//Función para inicializar el reloj
function iniciarReloj()
{
    txtTiempo.innerHTML = "Tiempo transcurrido:";
    interval = setInterval(function() {
        relojTime(m,seg);
        if(time >= s){
            seg++;
        }

        if(seg>59){
            m = m + 1;
            seg = 0;
        }
        if(s == 179){
            m = 3;
            seg = 0;
        }
        if (s == time) {

            stopTimer();
            return;
        }
        movimientoRelojAnimacion(s);
        s++;
        flechaCambia(s);
    }, 1000);
}

function movimientoRelojAnimacion(s){
    $('.circle_animation').css('stroke-dashoffset', initialOffset-((s+1)*(initialOffset/time)));
}

function relojTime(m,seg)
{
    $('h2').text(m+"min"+seg+"s.");
}

/*
 var isSecureOrigin = location.protocol === 'https:' ||
 location.hostname === 'localhost';
 if (!isSecureOrigin) {
 alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
 '\n\nChanging protocol to HTTPS');
 location.protocol = 'HTTPS';
 }
 */
// Constraints of video recording
var constraints = {
    audio: true,
    video: { width: 1080, height: 720 }
};

navigator.mediaDevices.getUserMedia(constraints).
then(handleSuccess).catch(handleError);

// If camera was correctly selected
function handleSuccess(stream) {
    recordButton.disabled = false;
    window.stream = stream;
    gumVideo.srcObject = stream;
}

// If camera it wasn't
function handleError(error) {
    console.log('navigator.getUserMedia error: ', error);
}


function handleSourceOpen(event) {
    console.log('MediaSource opened');
    sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
    console.log('Source buffer: ', sourceBuffer);
}

// Record video needs a listener, because event stop, pause, etc
recordedVideo.addEventListener('error', function(ev)
{
    console.error('MediaRecording.recordedMedia.error()');
    alert('Your browser can not play\n\n' + recordedVideo.src
        + '\n\n media clip. event: ' + JSON.stringify(ev));
}, true);

function handleDataAvailable(event)
{
    if (event.data && event.data.size > 0)
    {
        recordedBlobs.push(event.data);
    }
}

// Scalability
function handleStop(event)
{
    console.log('Recorder stopped: ', event);
}

// This function handles the video recording, web elements changes to notify user
function toggleRecording()
{

    if(recordButton.innerText === 'Iniciar grabación')
    {
        startRecording();
    }else{

        //Detiene la graación
        stopRecording();

        //Detiene el reloj
        stopTimer();

        gumVideo.style.display = 'none';
        recordedVideo.style.display = 'inline-block';
        recordButton.style.display = 'none';
        playButton.style.display = 'block';
        recordButton.innerText = 'Iniciar grabación';
        //downloadButton.disabled = false;
    }
}

// Functions that loads the recording configuration
function startRecording()
{
    recordedBlobs = [];
    var options = {mimeType: 'video/webm;codecs=vp9'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.log(options.mimeType + ' is not Supported');
        options = {mimeType: 'video/webm;codecs=vp8'};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.log(options.mimeType + ' is not Supported');
            options = {mimeType: 'video/webm'};
            if (!MediaRecorder.isTypeSupported(options.mimeType)) {
                console.log(options.mimeType + ' is not Supported');
                options = {mimeType: ''};
            }
        }
    }
    try {
        mediaRecorder = new MediaRecorder(window.stream, options);
    } catch (e) {
        console.error('Exception while creating MediaRecorder: ' + e);
        alert('Exception while creating MediaRecorder: '
            + e + '. mimeType: ' + options.mimeType);
        return;
    }

    //Inicializa el reloj
    iniciarReloj();

    recordButton.textContent = 'Detener grabación';
    btnIcon2.className = "stop icon" ;
    //playButton.disabled = true;
    //downloadButton.disabled = true;
    //uploadButton.disabled = true;
    mediaRecorder.onstop = handleStop;
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start(10); // collect 10ms of data
    console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording()
{
    mediaRecorder.stop();
    recordedVideo.controls = true;
}

// Handles playing recorded video
function play()
{
    var superBuffer = new Blob(recordedBlobs, {type: 'video/mp4'});
    recordedVideo.src = window.URL.createObjectURL(superBuffer);
    recordedVideo.addEventListener('loadedmetadata', function() {
        if (recordedVideo.duration === Infinity) {
            recordedVideo.currentTime = 1e101;
            recordedVideo.ontimeupdate = function() {
                recordedVideo.currentTime = 0;
                recordedVideo.ontimeupdate = function() {
                    delete recordedVideo.ontimeupdate;
                    recordedVideo.play();
                };
            };
        }
    });
}

// This function uploads the recorded video to the server
function uploadServer()
{
    var blob = new Blob(recordedBlobs, {type: 'video/webm'});
    var APIurl = "http://127.0.0.1:5000/api/file";
    var name = "videoTesting.mp4";

    var data = new FormData();
    data.append('userVideo', blob);

    $.ajax({
        url :  APIurl,
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        mimeType: "multipart/form-data",
        success: function(data) {
            console.log("yei");
        },
        error: function(error) {
            console.log("error");
            console.log(error);
        }
    });
}


/*
 // This function is executed when download button is pressed, downloads the video
 function download() {
 var blob = new Blob(recordedBlobs, {type: 'video/webm'});

 var url = window.URL.createObjectURL(blob);
 var a = document.createElement('a');
 a.style.display = 'none';
 a.href = url;
 a.download = 'test.webm';
 document.body.appendChild(a);
 a.click();
 setTimeout(function() {
 document.body.removeChild(a);
 window.URL.revokeObjectURL(url);
 }, 100);
 }
 */